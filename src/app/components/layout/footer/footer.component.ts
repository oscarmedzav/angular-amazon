import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {

  author: string = 'Oscar Medina';
  corp: string = 'Angular 8';
  date: string;

  constructor() { }

  ngOnInit() {
    this.date = new Date().toDateString();
  }

}
